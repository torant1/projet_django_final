﻿from django.conf.urls import patterns, url
from Resto_Lakay import views 


urlpatterns= patterns('',
				url(r'^index.html', views.index, name= 'index'),
				url(r'^menu.html', views.menu, name= 'menu'),
				url(r'^promotion.html', views.promotion, name= 'promotion'),
				url(r'^reservation.html', views.reservation, name= 'reservation'),
				url(r'^Entres.html', views.Entres, name= 'Entes'),
				url(r'^Mer.html', views.Mer, name= 'Mer'),
				url(r'^Riz.html', views.Riz, name= 'Riz'),
				url(r'^Viandes.html', views.Viandes, name= 'Viandes'),
				url(r'^inscrire.html', views.inscrire, name= 'inscrire'),
				url(r'^reservation.html', views.reservation, name= 'reservation'),
				url(r'^login.html', views.login, name= 'login'),
				url(r'^deconnexion/$', views.deconnexion, name= 'deconnexion'),
				)