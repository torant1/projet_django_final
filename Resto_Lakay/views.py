from django.shortcuts import render
from django import forms
from Resto_Lakay.forms import InscriptionForm, ReservationForm,loginForm
from Resto_Lakay.models import Clients,Reservation
from django.core.context_processors import csrf
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from django.contrib.auth import login as auth_login,logout ,authenticate
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse


def index(request):
	context_dict={'boldmessage':"Je suis serge Brunet et Je suis dans la vue"}
	return render(request, 'Resto_Lakay/index.html',context_dict)

def menu(request):
	context_dict={'boldmessage':"Je suis serge Brunet et Je suis dans la vue"}
	return render(request, 'Resto_Lakay/menu.html',context_dict)

def promotion(request):
	context_dict={'boldmessage':"Je suis serge Brunet et Je suis dans la vue"}
	return render(request, 'Resto_Lakay/promotion.html',context_dict)

def Entres(request):
	context_dict={'boldmessage':"Je suis serge Brunet et Je suis dans la vue"}
	return render(request, 'Resto_Lakay/Entres.html',context_dict)

def Mer(request):
	context_dict={'boldmessage':"Je suis serge Brunet et Je suis dans la vue"}
	return render(request, 'Resto_Lakay/Mer.html',context_dict)
def Riz(request):
	context_dict={'boldmessage':"Je suis serge Brunet et Je suis dans la vue"}
	return render(request, 'Resto_Lakay/Riz.html',context_dict)
	
def Viandes(request):
	context_dict={'boldmessage':"Je suis serge Brunet et Je suis dans la vue"}
	return render(request, 'Resto_Lakay/Viandes.html',context_dict)
	
def inscrire(request):
	registered = False
	error=False
	user_existe=False
	if request.method == 'POST':
		form = InscriptionForm(request.POST)
		if form.is_valid():		
			username=request.POST['Pseudo']
			prenom=request.POST['Prenom']
			nom=request.POST['Nom']
			telephone=request.POST['Telephone']
			adresse=request.POST['Adresse']
			sexe=request.POST['Sexe']
			mail=request.POST['Mail']
			password=request.POST['password']
			users= User.objects.all()
			for user in users:
				if user.username==username:
					registered=True
					break
			if not registered:
					user=User.objects.create_user(username=username,first_name=prenom,last_name=nom,email=mail,password=password)
					user.save()
					client=Clients(user=user,Telephone=telephone,Adresse=adresse,Sexe=sexe)
					client.save()
					return render(request, 'Resto_Lakay/index.html',locals())	
			else:
					user_existe=True
		else:
			error= True;
	else:
		form = InscriptionForm()
	return render(request,'Resto_Lakay/inscrire.html',locals()) 


def reservation(request):
			error=False
			if request.method == "POST":
				form_reserver = ReservationForm(request.POST)
				if form_reserver.is_valid():
					#mail=User.objects.get(username)
					nombre_de_Place=request.POST['Place']
					date_reserver=request.POST['reserver']
					heure=request.POST.get['Heure']
					form_reserver=Reservation(nombre_de_Place,date_reserver,heure)
					form_reserver.save()
					return render(request,'Resto_Lakay/promotion.html',locals())
				else:
					error=True	 
			else:
				form_reserver=ReservationForm()
			return render(request,'Resto_Lakay/reservation.html',locals())

def login(request):
	error = False
	if request.method == "POST":
		form_login = loginForm(request.POST)
		if form_login.is_valid():
			username = request.POST["username"]  
			password = request.POST["password"]  
			user = authenticate(username=username,password=password) 
			 #Nous vérifions si les données sont correctes
			if user is not None:  # Si l'objet renvoyé n'est pas None
				auth_login(request,user) 
			else: #sinon une erreur sera affichée
				error = True
	else:
		form_login = loginForm()
	return render(request,'Resto_Lakay/login.html',locals())
# Create your views here.


def deconnexion(request):
    logout(request) 
    return render(request,'Resto_Lakay/index.html',locals()) 