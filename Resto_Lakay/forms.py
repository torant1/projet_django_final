from django import forms
from django.forms import ModelForm
from Resto_Lakay.models import Clients,Reservation
from datetime import date, time

#model pour client
SEXE_CHOIX=(('H','Homme',),
            ('F','Femme',))

class MyDateInput(forms.DateInput):
    input_type = 'date'

class MyDateTimeInput(forms.DateTimeInput):
    input_type = 'datetime'

class MyTimeInput(forms.TimeInput):
    input_type = 'time'

class MyTelephoneInput(forms.TextInput):
    input_type = 'tel'

NOMBRE_DE_PLACE_CHOIX=(('1','1'),
           ('2','2'),
           ('3','3'),
           ('4','4'),
           ('5','5'),
           ('6','6'),
           ('7','7'),
           ('8','8'),
           ('9','9'),
           ('10','10'),)


class InscriptionForm(forms.Form):
		Pseudo= forms.CharField(label='Votre Pseudo',widget=forms.TextInput(attrs={'class': 'special','placeholder': 'Pseudo'}))
		Prenom= forms.CharField(label='Votre Prenom',widget=forms.TextInput(attrs={'class': 'special','placeholder': 'Prenom'}))
		Nom = forms.CharField(label='Votre Nom',
					widget=forms.TextInput(attrs={'class': 'special','placeholder': 'Nom'}))
		Telephone= forms.CharField(label='Votre Telephone',
					widget=MyTelephoneInput(attrs={'class': 'special','placeholder': 'Telephone'}))
		Adresse = forms.CharField(label='Votre Adresse',
					widget=forms.TextInput(attrs={'class': 'special','placeholder': 'Adresse'}))
		Sexe =forms.ChoiceField(label='Votre Sexe',initial='H',widget=forms.RadioSelect,choices=SEXE_CHOIX)
		Mail=forms.EmailField(label='Votre Mail',
					widget=forms.EmailInput(attrs={'class': 'special','placeholder': 'Email'}))
		password =forms.CharField(label='Votre mot de passe',
					widget=forms.PasswordInput(attrs={'class': 'special','placeholder': 'mot de passe'}))
		retaper_Password = forms.CharField(label='Retapez Votre mot de passe',
					widget=forms.PasswordInput(attrs={'class': 'special','placeholder': 'mot de passe'}))
		def clean(self):
			cleaned_data=super(InscriptionForm,self).clean()
			password=cleaned_data.get('password')
			retaper_Password=cleaned_data.get('retaper_Password')	
			if password and retaper_Password:
				if password != retaper_Password:
					raise forms.ValidationError("les mots de passe ne sont pas identiques")
			return cleaned_data				
		

class ReservationForm(forms.Form):
		#username=forms.CharField(label='Votre Pseudo',required=True,widget=forms.TextInput(attrs={'class': 'special','placeholder': 'Pseudo'}))
		Place =forms.CharField(label='Nombre de place' ,required=True,
			widget=forms.Select(choices=NOMBRE_DE_PLACE_CHOIX))
		reserver=forms.DateField( label=' Date de reservation',required=True,
			widget = MyDateInput())
		Heure=forms.TimeField(label='Heure de reservation',required=True,
		 	widget=MyTimeInput())


class loginForm(forms.Form):
	username=forms.CharField(label='Votre Pseudo',required=True,widget=forms.TextInput(attrs={'class': 'special','placeholder': 'Pseudo'}))
	password=forms.CharField(label='Votre mot de passe',required=True,
					widget=forms.PasswordInput(attrs={'class': 'special','placeholder': 'mot de passe'}))	  
# Create your models here.
