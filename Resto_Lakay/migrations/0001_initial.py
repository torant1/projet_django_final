# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Clients',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Telephone', models.CharField(max_length=200, default=None, null=True)),
                ('Adresse', models.CharField(max_length=200, default=None, null=True)),
                ('Sexe', models.CharField(max_length=10, null=True)),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Reservation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('Nombre_de_Place', models.CharField(max_length=2, default=None, null=True)),
                ('Date_reserver', models.DateField(default=None, null=True)),
                ('Heure', models.TimeField(default=None, null=True)),
                ('Mail', models.ForeignKey(to='Resto_Lakay.Clients')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
