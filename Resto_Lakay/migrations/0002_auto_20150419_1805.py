# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Resto_Lakay', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reservation',
            name='Date_reserver',
            field=models.DateField(default=None),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='reservation',
            name='Heure',
            field=models.TimeField(default=None),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='reservation',
            name='Nombre_de_Place',
            field=models.CharField(max_length=2, default=None),
            preserve_default=True,
        ),
    ]
